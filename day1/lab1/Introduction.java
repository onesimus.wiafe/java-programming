public class Introduction {
//    q1
    public static void helloWorld(){
        System.out.println("Hello, world!");
    }

//    q2.
    public static String tempNumber(int num){
        if (num % 2 == 0) {
            System.out.println(num + " is even");
        }else {
            System.out.println(num + " is odd");
        }
        if (num == 0){
            return "zero";
        }
        else if (num > 0 && num <= 14)
        {
            return "cold";
        }
        else if (num > 14 && num <=24)
        {
            return "cool";
        }
        else if (num > 24 && num <= 40)
        {
            return "warm";
        }
        else if (num > 40 && num <= 60){
            return "hot";
        }
        else if (num > 60 && num <= 80){
            return "very hot";
        }
        else if (num > 80 && num <= 99)
        {
            return "extremely hot";
        }
        return "boiling";
    }

//    q3.
    public static String toWord(){
        int num = (int)(Math.random()*10);

        switch (num){
            case 0 -> {
                return "zero";
            }
            case 1 -> {
                return "one";
            }
            case 2 -> {
                return "two";
            }
            case 3 -> {
                return "three";
            }
            case 4 -> {
                return "four";
            }
            case 5 -> {
                return "five";
            }
            case 6 -> {
                return "six";
            }
            case  7 -> {
                return "seven";
            }
            case 8 -> {
                return "eight";
            }
            case 9 -> {
                return  "nine";
            }
        }
        return "out of range";
    }

//    q4.
    public static void notZero(){
        int num;
        do {
            num = (int)(Math.random()*7)-3;
            System.out.println(num);
        } while (num != 0);

    }

    public static void notZeroRevised(){
        int num = (int)(Math.random()*7)-3;

        while (num != 0){
            System.out.println(num);
            num = (int)(Math.random()*7)-3;
        }
    }

//    q5.
    public static void multiTable(){
        for (int i = 1; i <= 10; i++)
        {
            for (int j = 1; j <= 12; j++){
                System.out.println(i + " * " + j + " = " + i*j);
            }
            System.out.println();
        }
    }

//    q6.
    public static void sixNumbers(){
        for (int i = 1; i <= 6; i++)
        {
            int num = (int)(Math.random()*50)+1;
            System.out.println(num);
        }
    }
    
    public static void sixNumbersRevised(){
	    for (int i = 1; i <= 6; i++)
	    {
		    int n = randomInteger(49);
		    System.out.println(n);
	    }
    }

    public static int randomInteger(int n){
	    int val = (int)(Math.random()*n);
	    return val;
    }
    
    public static void main(String[] args) {
        helloWorld();
        System.out.println();

        int rand = (int)(Math.random()*100)+1;
        System.out.println(tempNumber(rand));
        System.out.println();

        System.out.println(toWord());
        System.out.println();

        System.out.println("Not Zero");
        notZero();
        System.out.println("Not Zero Revised");
        notZeroRevised();
        System.out.println();

        multiTable();
        System.out.println();

	System.out.println("Six Numbers");
        sixNumbers();
	System.out.println("Six Numbers Revised");
	sixNumbersRevised();
        System.out.println();
    }
}
