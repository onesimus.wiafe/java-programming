public class Account {
    private double value;

    public Account(){
        this.value = 0.0;
    }

    public double getValue(){
        return this.value;
    }

    public void setValue(double value){
        this.value = value;
    }
}
