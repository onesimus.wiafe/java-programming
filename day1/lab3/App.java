public class App {
    public static void main(String[] args){
        Trade trade = new Trade("T1", "IBM", 100, 120.50);
        System.out.println(trade);

        Trade trade1 = new Trade("T2", "APPL", 120);
        trade1.setPrice(200);
        System.out.println(trade1);
        trade1.setPrice(-12.44);
        System.out.println(trade1);
    }

}
