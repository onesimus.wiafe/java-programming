public class Trade {
    private final String id;

    private final String symbol;
    private int quantity;
    private double price;

    public Trade(String id, String symbol, int quantity, double price){
        this.symbol = symbol;
        this.id = id;
        this.quantity = quantity;
        this.price = price;
    }

    public Trade (String id, String symbol, int quantity){
        this.symbol = symbol;
        this.id = id;
        this.quantity = quantity;
        this.price = 0.0;
    }


    //    setter for Trade price
    public void setPrice(double price){
        if (price > 0){
            this.price = price;
        }
    }

    public double getPrice(){
        return this.price;
    }

    public int getQuantity() {
        return  this.quantity;
    }

    public void setQuantity(int quantity){ this.quantity = quantity; }

    public String toString(){
        return this.id + ": " + this.symbol + " -> " + this.quantity + " shares at $" + this.price;
    }

}