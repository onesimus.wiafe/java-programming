import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TradeTest {

    @Test
    void setPriceTest() {
        System.out.println("Testing setPrice() of Trade class...");
        Trade trade = new Trade("T1", "IBM", 100, 120.50);
        trade.setPrice(13.41);
        System.out.println("Test 1: price = 13.41");
        assertTrue((13.41 == trade.getPrice()));
        System.out.println("Test 2: price = -123.2");
        trade.setPrice(-123.2);
        assertNotEquals(-123.2, trade.getPrice(), 0.0);
    }
}