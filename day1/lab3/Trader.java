public class Trader {
    private final String name;
    private final Account account;

    public Trader(String name){
        this.name = name;
        this.account = new Account();
    }

    public void addTrade(Trade trade){
        double value = trade.getPrice() * trade.getQuantity();
        double totalValue = value + account.getValue();
        account.setValue(totalValue);
    }

    public double getAccountValue() {
        return this.account.getValue();
    }

    public String getName() {
        return name;
    }
}
