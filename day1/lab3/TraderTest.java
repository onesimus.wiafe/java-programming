import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TraderTest {

    @Test
    void addTrade() {
        System.out.println("Testing addTrade() of Trader class...");
        Trade trade = new Trade("T1", "IBM", 100, 120.50);
        Trade trade1 = new Trade("T2", "APPL", 120);
        trade1.setPrice(200);

        Trader jl = new Trader("Jesse Livermore");
        double currentTotal = trade.getPrice() * trade.getQuantity();
        jl.addTrade(trade);

        System.out.println("Test 1: value = " + currentTotal);
        assertEquals(currentTotal, jl.getAccountValue(), currentTotal);
    }

    @Test
    void testAddTrade() {
        Trade trade1 = new Trade("T2", "APPL", 120);
        trade1.setPrice(200);

        Trader jl = new Trader("Jesse Livermore");

        double currentTotal = (trade1.getPrice() * trade1.getQuantity());
        System.out.println("Test 2: value = " + currentTotal);
        jl.addTrade(trade1);
        assertEquals(currentTotal, jl.getAccountValue(), currentTotal);
    }
}