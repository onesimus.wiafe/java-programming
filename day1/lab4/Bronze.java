public class Bronze extends MembershipType{

    public Bronze(String name, int points) {
        super(name, points);
    }

    public int getLimit(){
        return 5;
    }

    public String getName(){
        return "Bronze";
    }
}
