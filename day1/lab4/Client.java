import java.time.LocalTime;
import java.util.Objects;

public class Client {
    private final String firstName;
    private final String lastName;
    private MembershipType membershipType;
    private int points;

    private int trades;

    // constructor
    public Client(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        this.points = 0;
        this.membershipType = null;
        this.trades = 0;
    }

    // setter
    public void setPoints(int points){
        this.points = points + 1;
        this.getMembershipType().setPoints(this.points);
        this.upgradeClient();
    }

    public int getTrades(){
        return this.trades;
    }


    // getter
    public int getPoints(){
        return this.points;
    }

    public String getName(){
        return this.firstName + " " + this.lastName;
    }

    // getter for membershipType
    public MembershipType getMembershipType(){
        return this.membershipType;
    }

    // setter for  membershipType
    public void setMembershipType(int points){
        if (points < 10){

            this.membershipType = new Bronze("Bronze", points);
        }
        else if (points <=19){
            this.membershipType = new Silver("Silver", points);
        }
        else {
            this.membershipType = new Gold("Gold", points);
        }
    }

    // upgrade client
    public void upgradeClient(){
        int points = this.getPoints();
        switch (points){
            case 1, 10, 20 -> setMembershipType(points);
        }
    }

    public void addTrade(Trade T){}

    public String toString(){
        return this.getName() + " is a " + this.getMembershipType().getName() + " member with" + this.getMembershipType().getPoints() + " points.";
    }

    public void makeTrade(){
        if (this.canTrade()) {
            setPoints(this.points);
            this.trades += 1;
        }
    }

    public boolean canTrade(){
        int trades = this.getTrades();
        int limit = this.getMembershipType().getLimit();

        String membershipName = this.getMembershipType().getName();

        if (Objects.equals(membershipName, "Bronze") && LocalTime.now().isBefore(LocalTime.of(10, 0, 0))){
            return false;
        }

        return trades != limit;
    }
}
