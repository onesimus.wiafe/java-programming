public class FundTrade extends Trade1{

    private final double percentage;

    public FundTrade(String id, String symbol, int quantity, double price, double percentage){
        super(id, symbol, quantity, price);
        this.percentage = percentage;
    }

    private double getPercentage(){
        return this.percentage;
    }

    @Override
    public double calcDividend() {
        return this.getPrice() * this.getPercentage();
    }
}
