public class Gold extends MembershipType{
    public Gold(String name, int points) {
        super(name, points);
    }

    public int getLimit(){
        return 20;
    }

    public String getName(){
        return "Gold";
    }
}
