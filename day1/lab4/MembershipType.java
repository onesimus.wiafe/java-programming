public abstract class MembershipType {

    private int points;
    public MembershipType(String name, int points) {
        this.points = points;
    }

    public int getPoints(){
        return this.points;
    }

    public abstract int getLimit();

    public void setPoints(int points){
        this.points = points;
    }
    public abstract String getName();
}
