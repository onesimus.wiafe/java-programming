public class Silver extends MembershipType{
    public Silver(String name, int points) {
        super(name, points);
    }

    public int getLimit(){
        return 10;
    }

    public String getName(){
        return "Silver";
    }
}
